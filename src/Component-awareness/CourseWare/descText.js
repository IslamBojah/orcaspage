export const Dataretention = `
Data retention , also called records retention, is the continued storage of an organization&#39;s data for
compliance or business reasons.
Retention requirements exist for certain types of sensitive data or records, for example, sensitive
data being processed by a computer system, stored on media or accessed by a staffer. While
organizations are free to draft their own data retention policy, they must also adhere to a number of
data retention laws, especially if these organizations operate within regulated industries.
This course helps educate staff on how to properly create, retain and destroy electronic information.`;

export const Identitytheft = `
is the unauthorized collection of personal information and its subsequent use for criminal reasons
such as to open credit cards and bank accounts, redirect mail, set up cellphone service, rent vehicles
and even get a job.
There are many ways in which an individual&#39;s identity can be stolen, but people may be particularly
vulnerable to this crime online, where savvy criminals can gain access to personal information
through a number of avenues.
You’ll learn to spot and effectively react to identity thieves and, know what actions to take if you
become a victim.`;

export const Emailsecurity = `
Email security describes various techniques for keeping sensitive information in email
communication and accounts secure against unauthorized access, loss, or compromise. Email is a
popular medium for the spread of malware, spam, and phishing attacks, using deceptive messages
to entice recipients to divulge sensitive information, open attachments or click on hyperlinks that
install malware on the victim’s device. Email is also a common entry vector for attackers looking to
gain a foothold in an enterprise network and breach valuable company data.
In this course, you’ll learn how to keep yourself and your customers protected, how to recognize and
avoid email scams and threats, and how to employ email security best practices.`;


export const Humanerror = `
Human error means actions that were unintended or accidental. It is the leading cause of data and
security breaches. The most common types of breaches occur as a result of someone sending data to
the wrong person. With cyber criminals on the rise, not enough business owners are paying
attention to the avoidable consequences of human error.
This course will Looking complete online security awareness training solution.`;

export const Passwords = `
Passwords! They can be a source of frustration at times, but passwords are necessary to keep your
accounts safe and to prove that you are who you say you are. This course will help you understand
the importance of creating a strong password – one that’s both easy to remember and difficult to
guess. You’ll also learn what you need to do should you suspect your password has been
compromised.`;    




