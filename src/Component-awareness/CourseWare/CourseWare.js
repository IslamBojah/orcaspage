import React,{Component} from 'react'
import "./CourseWare.css"
import Password_logo from '../../images/password.png'
import Id_logo from '../../images/id.png'
import Human_logo from '../../images/human.png'
import Email_logo from '../../images/email.png'
import Data_logo from '../../images/data.png'


import {Dataretention,Identitytheft,Emailsecurity,Humanerror,Passwords} from "./descText"
export default  class CourseWare extends Component {

    state={
        decrp:Dataretention,
        Title:"Data Retention"
    }
    _handleChangeDesc=(x)=>{
        
    
        switch(x) {
            case "1":
               this.setState({decrp:Dataretention,Title:"Data Retention"})
              break;
            case "2":
                    this.setState({decrp:Identitytheft,Title:"Identity Theft"})
              break;
              case "3":
                    this.setState({decrp:Emailsecurity,Title:"Email Security"})
              break;
              case "4":
                    this.setState({decrp:Humanerror,Title:"Human Error"})
              break;
              case "5":
                    this.setState({decrp:Passwords,Title:"Passwords"})
              break;
            default:
              // code block
          }
      
    }

    render(){
      
    return (
        <div className="Main_coure">
            <div className="Coure_Upper">
                <text className='textSection' style={{color:'#f6f2f4'}} >Courseware</text>
            </div>
            <div className="Coure_Middle">

                <div className="Coure_Middle_Icon">
                    <div className="Coure_Middle_Icon-left">
                        <img src={Data_logo} alt="Dataretention" onClick={()=>this._handleChangeDesc("1")}></img>
                        <img src={Id_logo} alt="Identitytheft" onClick={()=>this._handleChangeDesc("2")}></img>
                        <img src={Email_logo} alt="Emailsecurity" onClick={()=>this._handleChangeDesc("3")}></img>
                     </div>
                    <div  className="Coure_Middle_Icon-right">
                        <img src={Human_logo} alt="Humanerror"    onClick={()=>this._handleChangeDesc("4")}></img>
                        <img src={Password_logo} alt="Passwords"     onClick={()=>this._handleChangeDesc("5")}></img>
                    </div>
                </div>

                <div className="Coure_Middle_Title"><p>{this.state.Title}</p></div>
            </div>
            <div className="Coure_Lower">
                <p>{this.state.decrp}</p>
            </div>
        </div>
    )
    }
}
