import React,{Component} from 'react'
import NavBar from '../Component/NavBar/NavBar'
import ImageSection from "./ImageSection/ImageSection"
import CardSection from "./CardSection/CardSection"
import CourseWare from "./CourseWare/CourseWare"
import  WhyWareness from "./WhyWareness/WhyWareness"
import Footer from "../Component/Footer/Footer"
class AwarenessHome extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (  
            <div style={{backgroundColor:'#f6f2f4'}}>
                <NavBar page='awareness'></NavBar>
                <ImageSection></ImageSection>
                <CardSection></CardSection>
                <CourseWare></CourseWare>
                <WhyWareness></WhyWareness>
                <Footer page='awareness'></Footer>
            </div>

         );
    }
}
 
export default AwarenessHome;