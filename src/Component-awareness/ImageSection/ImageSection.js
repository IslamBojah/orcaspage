
import React from 'react'
import "./ImageSection.css"
import {Button} from "react-bootstrap"
import {Link} from 'react-router-dom';

export default function ImageSection() {
    return (
        <div>
               <div className="slide-section">
                    <div className="Content_slide">

                        <div className="empty-side"></div>

                        <div className="content-side">

                            <div className="image_Title" >
                                
                                <img src={""}></img>
                                <h1>Orcas awareness</h1>
                           </div>

                           <h4>protect your company</h4>
                            <Link to='/contactUs'><Button >Contact Us</Button></Link>
                            
                        </div>
                        
                        <div className="right-side-awaHome"></div>
                    </div>
                </div>
        </div>
    )
}
