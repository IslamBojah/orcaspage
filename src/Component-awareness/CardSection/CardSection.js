import React from 'react'
import {Card,CardGroup} from 'react-bootstrap'
import BackImage from '../../images/awareHome.jpg'
import "./CardSection.css"

export default (props) => {
  return (
        <CardGroup className='CardGroup' >
           
            <Card className="p-3 Card"  >
                <text className='textSection3' style={{color:"#f96714"}}>Orcas awareness</text>
                <p>
                More than 80% of all cyber-incidents are caused by human error. Enterprises lose millions recovering from staff-related incidents – 
                but traditional training programs usually fail to achieve the desired behavioral changes and motivation. 
                Understanding what lies behind any learning and teaching process helps to build an effective educational program.
                Our programs not only deliver knowledge, 
                but – more importantly – change habits and form the new behavior patterns that are the real goal of awareness training.
                </p>
            </Card>
            <Card className='Card'>
                <Card.Img variant="top" src={BackImage} className='CardImage'/>
            </Card>
        </CardGroup>
  )
}
