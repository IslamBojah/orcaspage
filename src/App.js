import React ,{Component}from 'react';
import logo from './logo.svg';
import './App.css';

import Carousel from './Component/Carousels' 
import AboutUs from './Component/aboutUs/aboutUs'
import OurServices from './Component/ourServices'
import NavBar from './Component/NavBar/NavBar'
import OurTeam from './Component/OurTeam/OurTeam'
import Footer from './Component/Footer/Footer'


class App extends Component {

  render() {
    return (
      <div className='App'>
        <NavBar contactUs='/contactUs' page='Home'></NavBar>
        <Carousel id='#carousal'></Carousel>
        <AboutUs id='#aboutUs'></AboutUs>
        <OurServices id='#ourServices'></OurServices>
        <OurTeam></OurTeam>
        <Footer></Footer>
      </div>
    );
  }
}

export default App;

