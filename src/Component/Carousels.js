import React from 'react'
import { Carousel} from 'react-bootstrap';
import slider1 from "../images/slider1.jpg"
import slider2 from "../images/slider2.jpg"
import slider3 from "../images/slide-100.jpg"

export default function Carousels(props) {
        const {id}=props
        return (

            <Carousel id={id} interval={3000} >
                    <Carousel.Item>
                        <img src={slider1}  alt="orcas-company" className="d-block w-100" ></img>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img src={slider2}  alt="orcas-company" className="d-block w-100" ></img>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img src={slider3}  alt="orcas-company" className="d-block w-100" ></img>
                    </Carousel.Item>

            </Carousel>
    );
  }
  
 