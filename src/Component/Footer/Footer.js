import React from 'react'
import './Footer.css'

export default (props) => {
  return (
    <footer class={props.page=='awareness'?"app-footer1" :"app-footer"}>
            <span>Copyright &copy; 2019 ORCAS TECHNOLOGIES LTD. All Rights Reserved.</span>
    </footer>
  )
}
