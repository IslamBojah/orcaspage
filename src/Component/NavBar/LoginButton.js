import React from 'react'
import { Dropdown} from 'react-bootstrap'
import './NavBar.css';
import Logo from '../../images/Hexa.png'
import modal from '../Modal'

export default (props) => {
  return (
    <Dropdown  className='btn_Login' style={{backgroundColor:'#f6f2f4' , color:'#25c9d0'}} >
                            <Dropdown.Toggle  className='btn_toggle' style={{backgroundColor:'#f6f2f4' , color:'#25c9d0'}} bsPrefix='super' >
                                <p>Log In <i className="down"></i></p>

                            </Dropdown.Toggle>
                            <Dropdown.Menu className='btn_menu'  >
                                <Dropdown.Item  className='btn_item' bsPrefix='super' onClick={props.onShowLogin}>
                                    <img
                                        alt=""
                                        src={Logo}
                                        width="30"
                                        height="40"
                                        style={{flex:2 }}
                                    />
                                    <p  style={{flex:8 ,marginLeft:'5px' , color:'#3e3c47' }} >cyber security</p>
                                </Dropdown.Item>    
                            </Dropdown.Menu>
                        </Dropdown>
  )
}
