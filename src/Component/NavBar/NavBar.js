import React from 'react'
import { Nav , Navbar ,Button ,Dropdown , ButtonToolbar ,SplitButton  ,DropdownButton} from 'react-bootstrap'
import {Link } from 'react-scroll'
import Logo from '../../images/logo.png'
import './NavBar.css';
import {Link as Li } from 'react-router-dom';
import Modal from '../Modal'
import Login from './LoginButton'
import {connect} from 'react-redux'

class NavBar extends React.Component {
    state ={
        isHide:false,
        showToggle:false,
        modalShow:false,
    }
    hideBar = () => {
        const { isHide } = this.state
 
        window.scrollY > 0.0 ?
        !isHide && this.setState({ isHide: true })
        :
        isHide && this.setState({ isHide: false });
    }

    componentDidMount(){
         window.addEventListener('scroll', this.hideBar);
    }

    componentWillUnmount(){
          window.removeEventListener('scroll', this.hideBar);
    }
    _HandlerContactUs=(x)=>{
        this.setState({modalShow:true})
        
    }
    _goToHomeSection = ()=>{
        this.setState({isHide:false})
        window.addEventListener('scroll', this.hideBar);
        window.scrollTo(0, 0);
        this.setState({showToggle:false})
    }
    _showLoginModal  =() => {
        this.setState({modalShow:true})
       // this.props.Test2({test:'sabf'})
      }
    _HideLoginModal  =(x) => {
        this.setState({modalShow:false})
      }

    render() {
        return (
            <div className='Header'>
                {(this.state.isHide===false && this.props.page==='Home') &&
                <Navbar expand="lg"  className='navbar-top-default'  >
                    <Navbar.Brand href={this.props.aboutUs}>
                        <img
                        alt=""
                        src={Logo}
                        width="130"
                        height="40"
                        />
                       
                    </Navbar.Brand>
                     
                   <Nav  className="ml-auto" id='navbar' >
                        <Login onShowLogin={this._showLoginModal} ></Login>
                    </Nav> 
                     
                </Navbar>
                }
                {(this.state.isHide===false && this.props.page==='awareness') &&
                <Navbar expand="lg"  className='navbar-top-default'  >
                    <Navbar.Brand href={this.props.aboutUs}>
                        <img
                        alt=""
                        src={Logo}
                        width="130"
                        height="40"
                        />
                    </Navbar.Brand>
                    <Nav  className="ml-auto" id='navbar' >
                        <button className='btn_login_awareness' onClick={this._showLoginModal}> Log In </button>
                    </Nav> 
                    
                </Navbar>
                }
                <Navbar collapseOnSelect expand="lg"  className={(this.state.isHide || this.props.page!='Home')  ?'navbar-default-scroll':'navbar-default'} expanded={this.state.showToggle}>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" onClick={()=>this.setState({showToggle:!this.state.showToggle})} />
                    <Navbar.Collapse id="responsive-navbar-nav"  >
                        {this.props.page==='Home'  ?
                        <Nav className="mr-auto">
                            <Link  spy={true}   duration={500} smooth={true} onClick={this._goToHomeSection} ><Nav.Link  className='nav-link'>Home</Nav.Link></Link> 
                            <Link to='#aboutUs' spy={true}  offset={-130} duration={500} smooth={true} onClick={()=>this.setState({showToggle:false})}><Nav.Link className='nav-link' >About Us</Nav.Link></Link>                            
                            <Link to='#ourServices' spy={true}  offset={-100} duration={500} smooth={true} onClick={()=>this.setState({showToggle:false})}><Nav.Link className='nav-link' >Services</Nav.Link></Link>  
                            <Li to={this.props.contactUs} className='nav-link'>Contact Us</Li>           
                        </Nav>
                        :
                        <Nav  className="mr-auto BottomHeader"  >                                
                            <Li to='/' className={this.props.page==='awareness' ?'nav-link-awareness':'nav-link'} style={{width:'100%' , textDecoration:'none'}}  >Home</Li>           
                        </Nav>
                        
                        }
                    </Navbar.Collapse>
                  
                    {this.state.showToggle===false && 
                    <Nav>
                        {(this.state.isHide===true  && (this.props.page ==='Home' || this.props.page ==='awareness'))   &&
                        <div onClick={this._goToHomeSection}>
                                <img
                                    alt=""
                                    src={Logo}
                                    width="130"
                                    height="40"
                                    className='Logo' 
                                />
                                </div>
                        }
                        {this.props.page ==='contactUs'  &&
                        <Li to='/'  style={{width:'100%'}}>     
                            <img
                                alt=""
                                src={Logo}
                                width="130"
                                height="40"
                                className='Logo' 
                            />
                        </Li> 
                        }
                    </Nav>
                    }
                </Navbar>
                {this.state.modalShow &&
                    <Modal show={this.state.modalShow} _HideLoginModal={this._HideLoginModal}></Modal>
                }
            </div>  
        );
    }
}


export default (NavBar)
