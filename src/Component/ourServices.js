import React from 'react'
import icon from "../images/icon.png";
import icon2 from "../images/icon2.png";
import icon3 from "../images/icon3.png";

export default function OurServices(props) {
    const {id}=props
    return (

        <div className="our-services" style={{width:'100%' , marginTop:'20px' }} id={id}>
                <div className="services-top">
                    <p>Our Services</p>
                </div>
                <div className="services-down">
                        <div className="left-services-down">    
                            <div className="top-servies-left">  
                                <div >
                                    <img src={icon} ></img>
                                </div>
                                <div className="Decp-Content">
                                    <h2>Cyber security</h2>
                                    <p>
                                    Since good security practices are needed to protect the information; we offer a security 
                                    awareness program to educate the individuals about security risks by demonstrating some good and bad behaviors in 
                                    different situations in order to create a secure-minded workforce,
                                     who will behave in a socially responsible manner to protect themselves and their company’s reputation and privacy.    
                                    </p>
                                </div>
                            </div>
                            
                            <div className="down-servies-right">
                                    <div >
                                        <img src={icon3} ></img>
                                    </div>   
                                  <div className="Decp-Content">
                                     <h2>Outsourcing </h2>
                                     <p> 
                                     When your business is running 24/7 to come up with innovative means to maximize its potential,
                                     outsourcing becomes a necessity not just a choice. We provide outsourcing services in software, mobile and web development. 
                                     Moreover, we offer quality assurance services as a part of the overall product development cycle or as a standalone service. 
                                     Our team’s skills include: React.js, node.js, PHP, HTML, CSS, JavaScript, JQuery, Ajax, Python, React-native, ionic, Android, IOS and MySQL.
                                     </p>
                                  </div>
                             </div>
                            
                        </div>
                        <div className="right-servies-down"> 

                             <div className="top-servies-Right">  
                                    <div >
                                        <img src={icon3} ></img>
                                    </div>                                 <div className="Decp-Content">
                                    <h2>Software Development</h2>
                                    <p>  Mobile Applications (Android & iOS) :   We offer end-to-end mobile development solutions, from planning to delivery.</p>
                                    <p>  Web Application Development :  Our team designs scalable and responsive web applications and develops them according to the customer needs.</p>
                                    
                                    </div>

                                </div>
                            {/*
                            <div className="down-servies-Right">
                                {/*
                                 <img src={Icon}></img>
                                  <div style={{display:"block",padding:10,textAlign:'justify'}}>
                                     <h2>Orcas Security Awarenes </h2>
                                     <p></p>
                                  </div>
                                */ }

                               
                             </div>
                        </div>

                </div>
    );
  }
  