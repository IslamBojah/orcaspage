import React from 'react'
import {Card,CardGroup} from 'react-bootstrap'
import BackImage from '../../images/asset5.jpg'
import './OurTeam.css';

export default (props) => {
  return (
        <CardGroup className='CardGroup' >
            <Card className='Card'>
                <Card.Img variant="top" src={BackImage} className='CardImage'/>
            </Card>
            <Card className="p-3 Card"  >
                <text className='textSection3'>Our Team</text>
                <p>
                    Our team members constantly work on developing and improving their skills
                    and abilities in order to improve services and performance to satisfy our
                    customers and maintain cooperation. Moreover, we believe that our job is
                    to listen, research and understand the requirements related to each individual
                    business.
                </p>
            </Card>
        </CardGroup>
  )
}
