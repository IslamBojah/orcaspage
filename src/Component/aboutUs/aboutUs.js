import React from 'react'
import {Container,Row,Col} from 'react-bootstrap';
import aboutImg from "../../images/aboutImage.jpg"
import {Card,CardGroup} from 'react-bootstrap'
import '../OurTeam/OurTeam.css'
export default function AboutUs(props) {
  
    return (

        <CardGroup className='CardGroup' id={props.id} >
            <Card className="p-3 Card"  >
                <text className='textSection3'>About Us</text>
                <p>
                ORCAS technologies Ltd. is an emerging software and cyber-security firm in Hebron, 
                Palestine. It was established to steer the Palestinian community through the next generation 
                of business innovation powered by technology, software development and cyber-security services.
                </p>
            </Card>
            <Card className='Card'>
                <Card.Img variant="top" src={aboutImg} className='CardImage'/>
            </Card>
        </CardGroup>
    );
  }
  
 