import React from 'react'
import './ContactUs.css'
import NavBar from '../NavBar/NavBar'
import Footer from '../Footer/Footer'
import {Form,Button} from 'react-bootstrap'
import * as emailjs from 'emailjs-com'

class ContactUs extends React.Component {
    state={
        name:'' ,
        email:'',
        subject:'',
        message:'',
        validated:false,
        validName:false ,
        validEmail:false ,
        validSubject:false ,
        validmessage:false ,

    }

    
    _handleChangeName = e =>{
        this.setState({name:e.target.value} ,
            ()=>{
                if(!(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/).test(this.state.name)){
                    this.setState({validName:false})
                }
                else{
                    this.setState({validName:true})

                }
            });
    }
    _handleChangeEmail = e =>{
        this.setState({email:e.target.value} , 
        ()=>{
            if(!(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(this.state.email)){
                this.setState({validEmail:false})
            }
            else{
                this.setState({validEmail:true})
            }
        }
        )
    }
    _handleChangeSubject = e =>{
        this.setState({subject:e.target.value} , 
        ()=>{
            if(!(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/).test(this.state.subject)){
                this.setState({validSubject:false})
            }
            else{
                this.setState({validSubject:true})
            }  
        }
        )
    }
    _handleChangeMessage = e =>{
        this.setState({message:e.target.value} , 
        ()=>{
            if(!(/^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z ]+[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z-_ ]*$/).test(this.state.message)){
                this.setState({validmessage:false})
            }
            else{
                this.setState({validmessage:true})
            }    
        }
        )
    }
    _handleSubmit = e => {
        e.preventDefault();
        if(  this.state.validName === true &&  this.state.validEmail === true &&  this.state.validSubject===true &&  this.state.validmessage===true ){
            //this.setState({validated:false})
            
        }
        else{
            //this.setState({validated:true})
        }
      };
      
    render() {
    return (
     
    <div>
        <NavBar contactUs='/contactUs' page='contactUs'></NavBar>
        <div className="contact-us"  >
            <div className="main-contact">
                <div className="header-contact">
                    <h3>CNOTACT US</h3>
                </div> 
                <div className="body-contact">
                    <div className="white-box">
                        <div className="right-box">
                            <h3>Send us your contact info</h3>

                            <Form noValidate validated={this.state.validated} onSubmit={this._handleSubmit}>
    
                                    <Form.Group controlId="validationCustom01">
                                        <Form.Control
                                            required
                                            placeholder="First name"
                                            onChange={this._handleChangeName}                             
                                            isValid={this.state.validName}
                                            isInvalid={this.state.name===""?false:!this.state.validName}
                                        />
                                       
                                    </Form.Group>

        
                                    <Form.Group controlId="validationCustom01">
                                        <Form.Control
                                            required
                                            placeholder="Email"
                                            onChange={this._handleChangeEmail}                             
                                            isValid={this.state.validEmail}
                                            isInvalid={this.state.email===""?false:!this.state.validEmail}
                                        
                                        />
                                       
                                    </Form.Group>

                                    <Form.Group controlId="validationCustom03">
                                        <Form.Control
                                            required
                                            type="text"   
                                            placeholder="Subject"
                                            onChange={this._handleChangeSubject}                             
                                            isValid={this.state.validSubject}
                                            isInvalid={this.state.subject===""?false:!this.state.validSubject}
                                        
                                        />
                                   
                                    </Form.Group>
                                    
                                    <Form.Group controlId="validationCustom03">
                                        <Form.Control
                                            as="textarea" 
                                            rows="3"  
                                            required
                                            placeholder="Message"
                                            onChange={this._handleChangeMessage}                             
                                            isValid={this.state.validmessage}
                                            isInvalid={this.state.message===""?false:!this.state.validmessage}
                                        
                                        />
                                    
                                    </Form.Group>



                                      <Button  className="btn btn-light" type="submit">Submit form</Button>
                          </Form>

                        </div>
                        <div className="middle-box"></div>
                        <div className="left-box">
                                <div className="left-side-contact">
                                    <h5>Hebron,Palestine</h5>
                                    <h5>Phone: +970 2-222-2246 </h5>
                                    <h5>Fax: +970 2- 222-2247 </h5>
                                    <h5>PS Mobile: +972 59-442-2548 </h5>
                                    <h5>IL Mobile: +972 52-561-832</h5>
                                </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        <Footer></Footer>
    </div>
    );
    }
}

export default ContactUs;