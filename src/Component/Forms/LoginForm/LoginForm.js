import React,{Component} from 'react';
import {Form,Button} from "react-bootstrap"
import './login.css'
import {Link, withRouter  } from 'react-router-dom';
import joi  from 'joi-browser'
import {LoginAuth,LoginAutStepTwo} from '../../../Actions/actions'
import {connect} from 'react-redux'
import InlineError from '../InlineError'
import auth from "../../../protecdRoute/auth"

class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            validEmail:false ,
            account:{
                username:'' ,
                password:''
            },
            errors:{username:"please Enter email or phone in correct way",password:""},
            checkInput:{
                username:false,
                password:false,
            },
            changeSubmit:0,
          }
        this.schema = {
            username: joi.string().email({ minDomainAtoms: 2 }),
            phoneNumber:joi.string().regex(/^[0-9]{9,15}$/),
            password: joi.string().not(""),
        }
    }

 
    handleChange =e =>{
        const account={...this.state.account};
        account[e.currentTarget.name]=e.currentTarget.value ;
        this.setState({account} )
        var name=e.currentTarget.name
       // const ee=this.validatedProperty({name:e.currentTarget.name ,value:e.currentTarget.value})
       this.setState(prevState => ({
        checkInput: {                   
            ...prevState.checkInput,    
            [name]: false    
        }
    }))
    }
    validate =() =>{
        
        const errors={};
        const {error}=joi.validate(this.state.account , this.schema);
        if(!error ) return null ;
        for(let item of error.details) errors[item.path[0]]=item.message;
        return errors
    }
    handleSubmitEmail = e =>{
        e.preventDefault()
        var isValidEmailError=this.validatedProperty({name:'username' ,value:this.state.account.username});
        var isValidphoneError=this.validatedProperty({name:'phoneNumber' ,value:this.state.account.username});
   
        if(!isValidEmailError || !isValidphoneError){
          
            this.props.Test2({username:this.state.account.username})
            .then(res=>{
                if(this.props.data.sucess===true  ){
                    this.setState({validEmail:true,changeSubmit:1})
                        if (this.props.data.numactive === 0) {
                            auth.login(()=>{
                                this.props.history.push('/Forget-Pssword',{username:this.state.account.username , id:this.props.data.id})
                            })
                         
                        } 
                }
                else{
                    this.setState(prevState => ({
                        checkInput: {                   
                            ...prevState,    
                            username: true    
                        },
                        errors:{...prevState,username:"invalid Email"}
                    }))
                }
            
            }) 
        }
        else{
            this.setState(prevState => ({
                checkInput: {                   
                    ...prevState.jasper,    
                    username: true    
                }
            }))
        }
    }
    validatedProperty = ({name , value}) =>{
        const obj={[name]:value};
        const schema={ [name] : this.schema[name]};
        const {error}=joi.validate(obj , schema);
        console.log(error)
        return error?error.details[0].message:null ;
    }

    handleFullLogin=(e)=>{
        e.preventDefault()
      
        var isValidEmailError=this.validatedProperty({name:'password' ,value:this.state.account.password});
        
        if (!isValidEmailError){
              this.props.LoginStepTwo(this.state.account).then(res=>{
                alert(this.props.data.auth)
                  if(this.props.data.auth ===true && this.props.data.islogin ===true ){
                    this.setState(prevState => ({
                    
                        errors:{...prevState,FormError:"this account is ready active"}
                    }))
                    
                  }else if(this.props.data.auth ===true && this.props.data.islogin ===false){
                        alert("welcome")
                  }
              })
        }else{
            
            
             
             this.setState(prevState => ({
                checkInput: {                   
                    ...prevState,    
                    password: true    
                },
                errors:{...prevState,password:isValidEmailError}
            }))
            
        }
        
    }
 
_handlergoToforget=()=>{
    auth.login(()=>{
        this.props.history.push('/Forget-Pssword',{username:this.state.account.username , id:this.props.data.id})

    })
}

    
    render() { 
        return ( 
          <div className="min_body">
            
                    <div className="left-side">
                        <h4>Sign Into Awareness </h4>
                    
                    <Form >
                        
                        <Form.Group controlId="Login-Form" >

                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={this.state.account.username}
                                onChange={this.handleChange}
                                name='username'
                                required
                                disabled={this.state.changeSubmit===0?false:true}
                                isInvalid={this.state.account.username===""?false:this.state.checkInput.username}>
                                </Form.Control>
                                <Form.Control.Feedback type="invalid">{this.state.errors.username}</Form.Control.Feedback>
          
      
                          </Form.Group>
                     
                        {this.state.validEmail===true ?
                        <div>
                            <Form.Group controlId="Login-Password">
                                <Form.Control 
                                    type="password" 
                                    placeholder="password"
                                    value={this.state.account.password}
                                    onChange={this.handleChange}
                                    name='password'
                                    required
                                    isInvalid={this.state.account.password===""?this.state.checkInput.password:false}>
                                  
                                </Form.Control>
                                  <Form.Control.Feedback type="invalid">{this.state.errors.password}</Form.Control.Feedback>
                                  
                               
                            </Form.Group>
                            
                            
                            <div className="left-sied-Forget-password">
                                <Form.Group style={{width:"60%"}} controlId="formBasicChecbox">
                                    <Form.Check type="checkbox" label="Check" />
                                </Form.Group>
                               
                           <div onClick={this._handlergoToforget}><h6 >Forget Password</h6></div> 
                                
                            </div>      
                           
                            <div className="left-sied-login-button">

                            <InlineError>{this.state.errors.FormError}</InlineError>
                                <Button   
                                    class="btn btn-light" 
                                    type="submit"
                                    onClick={this.handleFullLogin}
                                >
                                Log In
                                </Button>
                            
                            </div>
                          
                        </div>
                    :
                    <div className="left-sied-login-button1">
                        <Button class="btn btn-light" type="submit" onClick={this.handleSubmitEmail}>Next</Button> 
                    </div>
                    }
                   
                    
                    </Form>
                    
                </div>
                <div className="right-side"></div>

          </div>

         );
    }
}
 
const mapToProps = dispatch =>{
    return{
        Test2:(data)=>dispatch(LoginAuth(data)),
        LoginStepTwo:(data)=>dispatch(LoginAutStepTwo(data)),
 
    }

  }
const mapToState =state =>{

    return{
        
      data:state.auth.data
    }
  }

export default withRouter(connect(mapToState,mapToProps)(LoginForm))

