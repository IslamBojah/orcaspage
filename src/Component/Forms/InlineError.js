import React,{Component} from "react";


class InlineError extends Component{
  
    render(){
   
        return(
   <span style={{ color: "red" ,display:'inline-block',fontSize:"15px" }} > {this.props.children}</span>
        )
    }
}

export default InlineError