import React from 'react'
import {Modal,Button} from 'react-bootstrap'
import LoginForm from './Forms/LoginForm/LoginForm'
export default (props) => {
    
  return (
        <Modal
            
            show={props.show}
            onHide={()=>props._HideLoginModal(false)}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered={window.innerWidth<800&& true}
            >
          
            <Modal.Body >

              <Modal.Header closeButton  style={{padding:"5px",borderBottom:0}}>

                 

              </Modal.Header>

              <LoginForm></LoginForm>

       

            </Modal.Body>

          

          </Modal>
  )
}
