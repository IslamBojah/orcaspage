

import React from 'react'
import {Form} from "react-bootstrap"

const  Input= ({name,label,value,onChnage,Type,placeholder}) => {
    return (  
        <Form.Group controlId="Login-Form">

            <Form.Control
            value={value}
            onChange={onChnage}
            id={name}
            name={name}
            type={Type}
            placeholder={placeholder}/>
       
        </Form.Group>

    );
}
 
export default Input ;