import axios from 'axios' ;
const link='http://192.168.100.123:9090'

export default{
    LoginAuthApi:{
        LoginAuthApi:data=>
            axios.post(link+'/api/v2.0/login',data).then(res=>res)
    },
    LoginAuthStepTwo:{
        LoginAuthApi:data=>
            axios.post(link+'/api/v2.0/auth',data).then(res=>res)
    },
    SetPassApi:{
        SetPassApi:data=>
        axios.put(link+'/api/v2.0/setpass' ,data).then(res=>res)
    },
    VerificationCode:{
        VerificationCodAuth:data=>
            axios.put(link+'/api/v2.0/verifycode',data).then(res=>res)
    },
}