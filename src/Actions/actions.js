import {Test , Login,LoginStepTwo , SetPass ,VerifiCode} from './types'
import api from './api'
import QS from 'qs'
import { async } from 'q';

export const Test2 =(data)=>{
    return (dispatch)=>{
        dispatch({type:Test})
        api.TestTest.TestTest({username:data.username})   
        .then(resp=>TestTest(dispatch,resp.data))
    }
}
const TestTest=(dispatch,data)=>{
    dispatch({
        type:Test,
        data
    })
}

export const LoginAuth=  data =>{
   return async(dispatch)=>{
       await api.LoginAuthApi.LoginAuthApi(data)
       .then(resp=>LoginAuthData(dispatch , resp.data))
    }
}
const LoginAuthData =(dispatch , data)=>{
    dispatch({
        type:Login,
        data
    })
}

export const LoginAutStepTwo=  data =>{
    return async(dispatch)=>{
        await api.LoginAuthStepTwo.LoginAuthApi(data)
        .then(resp=>LoginAutStepTwoData(dispatch , resp.data))
     }
 }
 const LoginAutStepTwoData =(dispatch , data)=>{
     dispatch({
         type:LoginStepTwo,
         data
     })
 }
 

 export const SetPassAction = data =>{
    return async(dispatch)=>{
        await api.SetPassApi.SetPassApi(data)
        .then(resp=>SetPassFunction(dispatch , resp.data))
     }
 }
 const SetPassFunction =(dispatch , data) =>{
    dispatch({
        type:SetPass,
        data
    })
 }
 /////Verification Code Action 

 export const VerificatonCodeAction =  data =>{
    return async(dispatch)=>{
        await api.VerificationCode.VerificationCodAuth(data)
        .then(resp=>VerificatonCodeData(dispatch , resp.data))
     }
 }
 const VerificatonCodeData =(dispatch , data)=>{
  
     dispatch({
         type:VerifiCode,
         data
     })
 }
 

