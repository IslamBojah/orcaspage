
import React, { Component } from 'react'
import{Route,Switch} from "react-router-dom"
import App from './App'
import  ContactUs from "./Component/ContactUs/ContactUs"
import AwarenessHome from "./Component-awareness/AwarenessHome"
import PassCard from "./Component-ForgetPass/PssCard"
import {ProtectedRoute} from "./protecdRoute/protectRoute"
class RouteApp extends Component {

    render() {
        return (
            <Switch>

                <Route path="/" exact component={App} ></Route>
                <Route path="/contactUs" component={ContactUs}></Route>
                
                <Route path="/Home" component={ContactUs}></Route>

                <Route path="/Awareness" component={AwarenessHome}></Route>
                <ProtectedRoute path="/Forget-Pssword" component={PassCard}></ProtectedRoute>
                  

            </Switch>
        );  
    }
}

export default RouteApp;