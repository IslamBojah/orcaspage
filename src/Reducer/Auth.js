
import {Test , Login,LoginStepTwo , SetPass ,VerifiCode} from '../Actions/types'

const INITIAL_SATATE={ user:null ,loading:false ,error:''}
export default (state=INITIAL_SATATE ,action) =>{
    switch(action.type){
        case Login:
            return action.data
        case LoginStepTwo:
            return action.data
        case SetPass:
            return action.data
        case VerifiCode:
             return action.data
            
        default:
            return state
    }
}