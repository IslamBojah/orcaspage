import React,{Component} from 'react'
import joi  from 'joi-browser'
import {Form,Button} from "react-bootstrap"
import {connect} from 'react-redux'
import {SetPassAction} from '../../Actions/actions'


class ChangePass extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            passwords:{
                password:'',
                confirmPassword:'',
            },
            errors:{
                password:'',
                confirmPassword:''
            },
            checkInput:{
                password:false,
                confirmPassword:false
            }
         }     
        this.schema = {
            password:  joi.string().min(8).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/),
           
        }        
    }

    handleChange =e =>{
        const passwords={...this.state.passwords};
        passwords[e.currentTarget.name]=e.currentTarget.value ;

        const ee =this.validatedProperty({name:e.currentTarget.name , value : e.currentTarget.value})
        var name= e.currentTarget.name ;
        {ee?this.setState(prevState => ({
            errors: {...prevState.errors,[name]: ee},
            checkInput: {...prevState.checkInput,[name]: true},
        }))
        :
        this.setState(prevState => ({
            errors: {...prevState.errors,[name]:ee},
            checkInput: {...prevState.checkInput,[name]: false},
        }))
        }
        this.setState({passwords})
    }
    validatedProperty = ({name , value}) =>{
        const obj={[name]:value};
        if(name==='confirmPassword'){
            return this.state.passwords.password===value?null : "Passwords not matched"
        }
        const schema={ [name] : this.schema[name]};
        const {error}=joi.validate(obj , schema);
        return error?'Password must be with at least 1 uppercase char , at least 1 lowercase char , at least 1 special char , at least 1 digit':null ;
    }
    validate =() =>{
        const passwords={...this.state.passwords};
        const ee=this.validatedProperty({name:'password' , value:passwords.password})
        var errorPassword=ee===null?true:false
        const ee1=this.validatedProperty({name:'confirmPassword' , value:passwords.confirmPassword})
        const errorConfirmPassword=ee1===null?true:false
        if(!errorPassword===true ){
            this.setState(prevState => ({
                errors: {...prevState.errors,password:ee},
                checkInput: {...prevState.checkInput,password: true},

            }))
        }
        if(!errorConfirmPassword===true ){
            this.setState(prevState => ({
                errors: {...prevState.errors,confirmPassword:ee1},
                checkInput: {...prevState.checkInput,confirmPassword: true},

            }))
        }
        return (passwords.password!="" && passwords.confirmPassword!="")? (errorPassword && errorConfirmPassword)?true:false:false
    }
    submit =e =>{
        e.preventDefault();
        var res=this.validate()
        if(res){
            this.props.Test2({id:4 , password:this.state.passwords.password}).then(res=>{
                console.log(this.props.data)
            })
        }
    }
    render() { 
        return ( 
            <div className="FormForget-min">

                <h1>Change Password</h1>

                <Form>
                    <Form.Group controlId="Login-Form">
                        <Form.Control 
                            type="password" 
                            placeholder="New Pssword"
                            name='password'
                            value={this.state.passwords.password}
                            onChange={this.handleChange}
                            isInvalid={this.state.checkInput.password}
                        >
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">{this.state.errors.password}</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group controlId="Login-Form">
                        <Form.Control 
                            type="password"
                            placeholder="Confirm New password"
                            name='confirmPassword'
                            value={this.state.passwords.confirmPassword}
                            onChange={this.handleChange}
                            isInvalid={this.state.checkInput.confirmPassword}
                        >
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">{this.state.errors.confirmPassword}</Form.Control.Feedback>
                    </Form.Group>
                    <div className="FormForget-End">
                        <Button  style={{background:"transparent" ,color:"#f96714",borderColor:"#f96714"}} onClick={this.submit}>Change</Button>
                    
                    </div>
                </Form>
            </div>
         );
    }
}
 
const mapToProps = dispatch =>{
    return{
        Test2:(data)=>dispatch(SetPassAction(data)),
    }

  }
const mapToState =state =>{
    return{
      data:state.auth
    }
  }

export default connect(mapToState,mapToProps)(ChangePass)
