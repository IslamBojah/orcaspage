import React,{Component} from 'react'
import "./PssCard.css"
import {Card } from "react-bootstrap"
import Logo from "../images/awaLog.png"
import ForgetPassword from "./Forget_Form/ForgetForm" 
import ChangePass from "./ChangePss/ChangePass"

class PassCard extends Component {
    state={
        Next:0,
    }
 
    _handlerNextVerficiation=(verify)=>{
      
        this.setState({Next:verify})
      
    }
  
    render() { 
        return ( 
            <div className="passCard-Image"> 
                <div className="passCard-Min">
                     <Card >
                
                        <Card.Body>
                            
                                 <Card.Title>
                                     <img src={Logo}  style={{padding:10}}></img>

                                     Orcas awareness

                                </Card.Title>
                                {this.state.Next ===0 && 
                                <ForgetPassword NextVerify={this._handlerNextVerficiation} username={this.props.location.state.username} id={this.props.location.state.id} ></ForgetPassword>
                                }
                            
                             {this.state.Next ===1 &&
                                <ChangePass NextVerify={this._handlerNextVerficiation} ></ChangePass>
                            }
                            
                        </Card.Body>

                    </Card>
                </div>
            </div>
         );
    }
}
 
export default PassCard;