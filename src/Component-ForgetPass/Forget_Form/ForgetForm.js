import React,{Component} from 'react'
import "./ForgetForm.css"
import {Form,Button} from "react-bootstrap"
import Input from "../../Form/Input.js"
import joi  from 'joi-browser'
import {connect} from "react-redux"
import { withRouter,Link } from 'react-router-dom';
import {VerificatonCodeAction} from "../../Actions/actions"
class ForgetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            account:{
                email:this.props.username ,
                verCode:'',
                id:this.props.id
                
            },
            errors:{},
            checkEmpty:false
         }

        this.schema = {
            verCode: joi.string().not("").length(6),
        }
    } 
   
   
    handleChange =e =>{
        const account={...this.state.account};
        account[e.currentTarget.name]=e.currentTarget.value ;
        this.setState({account,checkEmpty:false})
        var name=e.currentTarget.name
       // const ee=this.validatedProperty({name:e.currentTarget.name ,value:e.currentTarget.value})
  
    }
    validate =() =>{
        const errors={};
        const {error}=joi.validate(this.state.account , this.schema);
        if(!error ) return null ;
        for(let item of error.details) errors[item.path[0]]=item.message;
        return errors
    }
    handleSubmitEmail = e =>{
        e.preventDefault()
          var isValidEmailError=this.validatedProperty({name:'verCode' ,value:this.state.account.verCode});
        
        if(isValidEmailError===null){
           
            this.props.VericicationApi(this.state.account).then(res=>{
                if(this.props.data.sucess ===false){
                
                    this.setState(prevState => ({
                    
                        errors:{...prevState,VerCodeError:"Enter Valid Code"},
                        checkEmpty:{...prevState,checkEmpty:true}
                    }))
                
                    
                    
                }else{
                    this.props.NextVerify(1)
                  
                }
                
            })

             
        }
        else{
            this.setState(prevState => ({
                    
                errors:{...prevState,VerCodeError:isValidEmailError},
                checkEmpty:{...prevState,checkEmpty:true}
            }))
        
            
        }
        
        
   

    }
    validatedProperty = ({name , value}) =>{
        const obj={[name]:value};
        const schema={ [name] : this.schema[name]};
        const {error}=joi.validate(obj , schema);
        console.log(error)
        return error?error.details[0].message:null ;
    }

    render() { 
        return ( 
            <div className="FormForget-min">

                <h1>Verification Code</h1>
                <h6>Please enter the sent code</h6>
            
                <Form>

                         <Form.Group controlId="Login-Form">
                            <Form.Control  placeholder="Verification Code" value={this.props.username} disabled={true}></Form.Control>
                        </Form.Group>
                          
                        <Form.Group controlId="Login-Password">

                                <Form.Control 
                                    type="text" 
                                    placeholder="Enter Verification Code"
                                    value={this.state.account.verCode}
                                    onChange={this.handleChange}
                                    name='verCode'
                                    required
                                    isInvalid={this.state.checkEmpty}>
                                 </Form.Control>

                                  <Form.Control.Feedback type="invalid">{this.state.errors.VerCodeError}</Form.Control.Feedback>
                                  
                               
                            </Form.Group>
                            

                        <div className="FormForget-End">
                       <Link to="/"><Button  style={{background:"transparent" ,color:"#f96714",borderColor:"#f96714"}}>Cancel</Button></Link>  
                         <Button   type="submit"  onClick={this.handleSubmitEmail}>Next</Button>
                        </div>

                </Form>

            </div>
         );
    }
}
const mapToProps = dispatch =>{
    return{
        VericicationApi:(data)=>dispatch(VerificatonCodeAction(data)),
     
 
    }

  }
const mapToState =state =>{
    return{
      data:state.auth.data
    }
  }

export default withRouter(connect(mapToState,mapToProps)(ForgetPassword))
